# KUBERNETES

## Divisão

- Master e Workes (antigo minios)

## Master

- Possui 4 componentes: ETCD, API Server, Controller Manager e Scheduler.

- Kubectl é um utilitário de linha de comando que se conecta ao servidor API.
  - Usado pelos adminis para criar pods, deployments, serviços, etc.

### ETCD

- Base de dados de chave valor;
- Armazena dados de configuração do cluster e estado do cluster.

### API Server

- Fornece API kubenetes usando JSON;
- Estados de objetos da API são armazenados no ETCD;
- O kubectl usa o API Server para se comunicar com o cluster.

### Controller Manager

- Monitora os controladores de replicação e cria os pods para manter o estado desejado.

### Scheduler

- Responsável por executar as tarefas de agendamento, como execução de contêineres nos workers (minions) com base na disponibilidade de recursos.





## Worker

- Nome dado para cada host do cluster (nós ou nodes);
- Máquinas que realizam as tarefas solicitadas.
- Possui os seguintes componentes: kubelet, docker, kube-proxy.

### Kubelet

- Agente executado em cada nó worker;
- Conecta-se ao Docker e cuida da criação, execução e exclusão de contêineres.


### Docker

- Responsável por rodar e executar os containeres.

### Kube-Proxy

- Encaminha o tráfego para os contêineres apropriados com base no endereço IP e no número da porta da solitação recebida.

## Outros Conceitos

### Pod

- Grupo de contêineres que são implantados em um único nó de trabalho;
- Menor unidade dentro de um cluster Kubernetes;
- Nada mais é do que containers executando dentro se seu cluster Kubernetes;
- Pode ser um container rodando um nginx, php, apache etc;
- Pod é a abstração de containers;
- Pode ser um ou mais containers em um Pod;
- Pode ter vários Pods dentro de cada nó.

### Replication Controller

- Responsável por manter um número determinado de pods em execução.
- É onde é dito quantos containers desejam-se que fiquem rodando para atender determinado serviço, caso um caia no Replication Controller, outra instância é criada automaticamente;
- Exemplo:
  - Em 3 réplicas do NGINX, o Replication Controller fica responsável por garantir o estado desse serviço, caso um caia, o RC sobe outro automaticamente.

### Services

- Atrela uma faixa de IP para um determinado Replication Controller;
- A cada que o RC cria uma nova instância de pod, ele inicia com um IP determinado pelo service;
- Services vai ser o ponto de entrada dos serviços.

### Namespace

- Com o Namespace pode-se dividir o Cluster de Kubernetes em dois ambientes, como Produção e Teste, podendo limitar os recursos computacionais para ambos.



### Demonsets

- Rodar sempre um ou mais podes por nó.

### Secrests

- Salvar dados sensitivos como senhas de bancos de dados.

### ConfigMaps

- Arquivo de configurações que as aplicações irão usar.


### Cron Jobs

- Executar tarefas temporais, uma vez ou repetida vezes.


## Minikube

- Iniciando
  > minikube start

- Obtendo nós
  > kubectl get nodes

- Obtendo pods do cluster
  > kubectl get pods

- Criar pod
  > kubectl create -f aplicação.yaml
    - f = file

- Delete pod
  > kubectl delete -f aplicação.yaml

- Listar deployments
  > kubectl get deployments

- Listar serviçes
  > kubectl get service

- Nome da url de acesso
  > minikube service <nome_serviço> --url

- Status minikube
  > minikube status

- UI Kubernetes
  > minikube dashboard


## Referências
  
  > git clone https://github.com/concrete-cristian-trucco/kubernetes-basico-nginx.git

- https://imasters.com.br/desenvolvimento/tudo-o-que-voce-precisa-saber-sobre-o-kubernetes-parte-02


